<%@ include file="/WEB-INF/views/includes/taglibs.jsp" %>

<spring:url scope="page" var="jqueryJavascriptUrl" value="/resources/js/jquery-1.7.1.js"/>
<spring:url scope="page" var="jqueryTmplJavascriptUrl" value="/resources/js/jquery.tmpl.min.js"/>
<spring:url scope="page" var="jqueryAtmosphereUrl" value="/resources/js/jquery.atmosphere.js"/>
<spring:url scope="page" var="bootstrapUrl" value="/resources/js/bootstrap.js"/>
<spring:url scope="page" var="bootstrapCssUrl" value="/resources/css/bootstrap.css"/>
<spring:url scope="page" var="bootstrapResponsiveCssUrl" value="/resources/css/bootstrap-responsive.css"/>

<!DOCTYPE HTML>
<html>
<head>
    <title>Welcome to Spring Web MVC - Atmosphere Sample</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>

    <script src="${pageScope.jqueryJavascriptUrl}"></script>
    <script src="${pageScope.jqueryTmplJavascriptUrl}"></script>
    <script src="${pageScope.jqueryAtmosphereUrl}"></script>
    <script src="${pageScope.bootstrapUrl}"></script>

    <link rel="stylesheet" href="${pageScope.bootstrapCssUrl}"/>
    <link rel="stylesheet" href="${pageScope.bootstrapResponsiveCssUrl}"/>

    <style>
        textarea#console {
            color: #ccc;
            background-color: #555;
            width: 400px;
            height: 300px;
            border: none;
            border-radius: 4px;
        }
    </style>
</head>
<body>
<button id="open">Open</button>
<button id="close">Close</button>
<textarea id="console" readonly="readonly"></textarea>

<script type="text/javascript">
    var c = {
        ref: $("textarea#console"),
        println: function(s){
            var c = this.ref;
            c.val(c.val()+s+"\n");
            c.scrollTop(c[0].scrollHeight-c.height());
        }
    };

    var socket = $.atmosphere;

    function subscribe() {
        var request = new $.atmosphere.AtmosphereRequest();
        request.transport = "streaming";
        request.url = "data";
        request.contentType = "application/json";
        request.fallbackTransport = null;
        request.maxRequest = 10000;

        request.onMessage = function (response) {
            //$.atmosphere.log('info', ["response.state: " + response.state]);
            //$.atmosphere.log('info', ["response.transport: " + response.transport]);
            //$.atmosphere.log('info', ["response.responseBody: " + response.responseBody]);

            if (response.state = "messageReceived") {
                var data = response.responseBody;
                if (data) {
                    try {
                        c.println(data);
                    }
                    catch (error) {
                        c.println("An error ocurred: "+ error);
                    }
                }
                else {
                    c.println("response.responseBody is null - ignoring.");
                }
            }
        };

        request.onMessagePublished = function (response) {
            c.println('message published');
        };

        request.onOpen = function () {
            c.println('socket open');
        };
        request.onError = function () {
            c.println('socket error');
        };
        request.onReconnect = function () {
            c.println('socket reconnect');
        };

        socket.subscribe(request);
    }

    $("button#open").click(function () {
        subscribe();
    });
    $("button#close").click(function () {
        socket.unsubscribe();
    });
</script>
</body>
</html>
